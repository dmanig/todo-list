import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {IAppState, rootReducer, INITIAL_STATE} from './store';
import {NgReduxModule, NgRedux} from '@angular-redux/store';
import {TodolistComponent} from './todolist/todolist.component';
import {TodoinputComponent} from './todoinput/todoinput.component';
import {TodoheaderComponent} from './todoheader/todoheader.component';
import {FormsModule} from '@angular/forms';
import {LabelinputComponent} from './labelinput/labelinput.component';
import {ClearbuttonComponent} from './clearbutton/clearbutton.component';
import {RemoveitemComponent} from './removeitem/removeitem.component';
import {CheckboxComponent} from './checkbox/checkbox.component';

@NgModule({
  declarations: [
    AppComponent,
    TodolistComponent,
    TodoinputComponent,
    TodoheaderComponent,
    LabelinputComponent,
    ClearbuttonComponent,
    RemoveitemComponent,
    CheckboxComponent
  ],
  imports: [
    BrowserModule, NgReduxModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>, ) {

    ngRedux.configureStore(rootReducer, INITIAL_STATE);
    ngRedux.subscribe(() => {
      const state = ngRedux.getState();


      console.log('State changed', state);
      for (const t of state.todos) {
        console.log('Todo', t.id, t.description, t.done);
      }
    });
  }

}
