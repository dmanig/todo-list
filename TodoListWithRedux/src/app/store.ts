
import {SWITCHDONE, ADD, CHANGEDESCRIPTION, CLEARLIST, REMOVEITEM, TODOSTATECHANGE} from './actions';
import {ITodo} from './totodata';
export interface IAppState {
  todos: ITodo[];
}

export const INITIAL_STATE: IAppState = {
  todos: []
};

export function rootReducer(state: IAppState, action): IAppState {
  switch (action.type) {
    case SWITCHDONE: return switchDone(state, action);
    case ADD: return add(state, action);
    case CHANGEDESCRIPTION: return changeDescr(state, action.body);
    case CLEARLIST: return clearList();
    case REMOVEITEM: return removeItem(state, action);
    case TODOSTATECHANGE: return stateChange(state, action.body);
    default: return state;
  }
}

function stateChange(state: IAppState, body) {

  const newstate: IAppState = {
    todos: []
  };
  for (const t of state.todos) {
    newstate.todos.push({
      ...t,
      done: (body.id === t.id) ? body.done : t.done
    });
  }
  return newstate;
}


function clearList() {

  const newstate: IAppState = {
    todos: []
  };
  return newstate;
}

function removeItem(state: IAppState, action) {

  const newstate: IAppState = {
    todos: []
  };
  for (const t of state.todos) {
    if (action.body !== t.id) {
      newstate.todos.push({
        ...t
      });
    }
  }
  return newstate;
}

function changeDescr(state: IAppState, body) {

  const newstate: IAppState = {
    todos: []
  };
  for (const t of state.todos) {
    newstate.todos.push({
      ...t,
      description: (body.id === t.id) ? body.text : t.description
    });
  }
  return newstate;
}

function switchDone(state: IAppState, action) {

  const newstate: IAppState = {
    todos: []
  };
  for (const t of state.todos) {
    newstate.todos.push({
      ...t,
      done: (action.body === t.id) ? !t.done : t.done
    });
  }
  return newstate;
}


function add(state: IAppState, action) {

  const newstate: IAppState = {
    todos: []
  };

  let lastId = 0;

  for (const t of state.todos) {
    if (t.id > lastId) {
      lastId = t.id;
    }
    newstate.todos.push({
      ...t
    });
  }

  newstate.todos.push({
    id: lastId + 1,
    done: false,
    description: action.body
  });
  return newstate;
}

