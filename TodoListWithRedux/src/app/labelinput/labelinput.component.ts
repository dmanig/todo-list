import {CHANGEDESCRIPTION} from '../actions';
import {IAppState} from '../store';
import {NgRedux} from '@angular-redux/store';
import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-labelinput',
  templateUrl: './labelinput.component.html',
  styleUrls: ['./labelinput.component.css']
})
export class LabelinputComponent implements OnInit {
  @Input('text') text = '';
  @Input('done') done = false;
  editMode = false;

  @Input('id') id: number;


  constructor(private ngRedux: NgRedux<IAppState>) {}

  ngOnInit() {
  }

  public editModeOn() {
    this.editMode = true;
  }

  public editModeOff() {
    this.editMode = false;
    this.ngRedux.dispatch({type: CHANGEDESCRIPTION, body: {id: this.id, text: this.text}});
  }
}
