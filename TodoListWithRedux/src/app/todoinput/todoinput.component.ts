import {ADD} from '../actions';
import {IAppState} from '../store';
import {NgRedux} from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-todoinput',
  templateUrl: './todoinput.component.html',
  styleUrls: ['./todoinput.component.css']
})
export class TodoinputComponent implements OnInit {
  newtodo = '';
  constructor(private ngRedux: NgRedux<IAppState>) {}

  ngOnInit() {
  }

  public add() {
    this.ngRedux.dispatch({type: ADD, body: this.newtodo});
    this.newtodo = '';
  }

}
