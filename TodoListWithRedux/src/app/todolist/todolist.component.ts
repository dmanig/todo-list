import {SWITCHDONE} from '../actions';
import {IAppState} from '../store';
import {ITodo} from '../totodata';
import {select, NgRedux} from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'app-todolist',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css']
})
export class TodolistComponent implements OnInit {
  @select() todos: Observable<ITodo[]>;
  constructor(private ngRedux: NgRedux<IAppState>) {}

  ngOnInit() {
  }

  public switchDone(id: number) {
    this.ngRedux.dispatch({type: SWITCHDONE, body: id});
  }

}
