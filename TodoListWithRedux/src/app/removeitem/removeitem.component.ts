import {REMOVEITEM} from '../actions';
import {IAppState} from '../store';
import {NgRedux} from '@angular-redux/store';
import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-removeitem',
  templateUrl: './removeitem.component.html',
  styleUrls: ['./removeitem.component.css']
})
export class RemoveitemComponent implements OnInit {
  confirmMode = false;
  @Input('id') id: number;
  constructor(private ngRedux: NgRedux<IAppState>) {}

  ngOnInit() {
  }



  public openConfirm() {
    this.confirmMode = true;
  }

  public confirmDelete() {
    this.confirmMode = false;
    this.ngRedux.dispatch({type: REMOVEITEM, body: this.id});
  }

  public closeConfirm() {
    this.confirmMode = false;
  }


}
