import {CLEARLIST} from '../actions';
import {IAppState} from '../store';
import {NgRedux} from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-clearbutton',
  templateUrl: './clearbutton.component.html',
  styleUrls: ['./clearbutton.component.css']
})
export class ClearbuttonComponent implements OnInit {
  confirmMode = false;
  constructor(private ngRedux: NgRedux<IAppState>) {}

  ngOnInit() {
  }


  public openConfirm() {
    this.confirmMode = true;
  }

  public confirmDelete() {
    this.confirmMode = false;
    this.ngRedux.dispatch({type: CLEARLIST});
  }

  public closeConfirm() {
    this.confirmMode = false;
  }

}
