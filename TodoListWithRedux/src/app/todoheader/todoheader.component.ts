import { select } from '@angular-redux/store';
import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-todoheader',
  templateUrl: './todoheader.component.html',
  styleUrls: ['./todoheader.component.css']
})
export class TodoheaderComponent implements OnInit {
  @select() todos;
  constructor() {}

  ngOnInit() {
  }

}
