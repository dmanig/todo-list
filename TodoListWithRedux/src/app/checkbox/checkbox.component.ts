import {TODOSTATECHANGE} from '../actions';
import {IAppState} from '../store';
import {NgRedux} from '@angular-redux/store';
import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent implements OnInit {
  @Input('checked') checked;
  @Input('id') id: number;
  @Input('text') text = '';


  constructor(private ngRedux: NgRedux<IAppState>) {}

  ngOnInit() {

  }

  public switchState() {
    this.checked = !this.checked;
    this.ngRedux.dispatch({type: TODOSTATECHANGE, body: {id: this.id, done: this.checked}});
  }

}
